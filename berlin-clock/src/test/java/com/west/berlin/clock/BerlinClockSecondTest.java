package com.west.berlin.clock;

import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.west.berlin.clock.lamp.Colour.YELLOW;
import static org.hamcrest.core.IsEqual.equalTo;

public class BerlinClockSecondTest {

    private BerlinClock berlinClock;

    @Before
    public void setup() {
        berlinClock = new BerlinClock();
    }

    @Test
    public void secondLampIlluminatesOnEvenSeconds() {
        IntStream.range(0,59).boxed().forEach(second -> verifySecondLamps(second));
    }

    @Test
    public void secondLampIsYellow() {
        assertThat(berlinClock.getSecondLamp().getColour(), equalTo(YELLOW));
    }

    private void verifySecondLamps(Integer second) {
        ZonedDateTime dateTime = ZonedDateTime.now().withSecond(second);

        if(second % 2 == 0) {
            assertThat(berlinClock.getSecondLamp().isOn(dateTime), is(true));
        } else {
            assertThat(berlinClock.getSecondLamp().isOn(dateTime), is(false));
        }
    }
}
