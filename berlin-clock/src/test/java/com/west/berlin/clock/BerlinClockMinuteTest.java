package com.west.berlin.clock;

import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import static com.west.berlin.clock.lamp.Colour.YELLOW;
import static com.west.berlin.clock.lamp.Colour.RED;

public class BerlinClockMinuteTest {

    private BerlinClock berlinClock;

    @Before
    public void setup() {
        berlinClock = new BerlinClock();
    }

    @Test
    public void topPanelMinuteLampsIlluminate() {
        IntStream.range(0,59).boxed().forEach(minute -> verifyMinuteLamps(minute));
    }

    @Test
    public void checkMinuteLampColours() {
       IntStream.rangeClosed(0,berlinClock.getTopMinutePanel().size()-1).boxed()
               .forEach(i -> {
                   boolean isRed = ((i+1) % 3) == 0;
                   if(isRed) {
                       assertThat(berlinClock.getTopMinutePanel().get(i).getColour(), equalTo(RED));
                   } else {
                       assertThat(berlinClock.getTopMinutePanel().get(i).getColour(), equalTo(YELLOW));
                   }
               });
    }

    private void verifyMinuteLamps(Integer minute) {
        ZonedDateTime dateTime = ZonedDateTime.now().withMinute(minute);
        berlinClock = new BerlinClock();

        int maxExpectedBlock = minute / 5;
        IntStream.range(0,Math.max(0,maxExpectedBlock-1)).forEach(i ->
        {
            assertThat(berlinClock.getTopMinutePanel().get(i).isOn(dateTime), is(true));
        });

        assertThat(berlinClock.getBottomMinutePanel().get(0).isOn(dateTime), is(minute % 5 == 1));
        assertThat(berlinClock.getBottomMinutePanel().get(1).isOn(dateTime), is(minute % 5 == 2));
        assertThat(berlinClock.getBottomMinutePanel().get(2).isOn(dateTime), is(minute % 5 == 3));
        assertThat(berlinClock.getBottomMinutePanel().get(3).isOn(dateTime), is(minute % 5 == 4));
    }
}
