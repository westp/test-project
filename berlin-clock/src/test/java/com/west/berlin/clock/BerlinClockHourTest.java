package com.west.berlin.clock;

import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.west.berlin.clock.lamp.Colour.RED;
import static org.hamcrest.core.IsEqual.equalTo;

public class BerlinClockHourTest {

    private BerlinClock berlinClock;

    @Before
    public void setup() {
        berlinClock = new BerlinClock();
    }

    @Test
    public void checkHourLampsIllumination() {
        IntStream.range(0,23).boxed().forEach(hour -> verifyHourLampIllumination(hour));
    }

    @Test
    public void checkHourLampColours() {
        berlinClock.getTopHourPanel().stream().forEach(lamp -> assertThat(lamp.getColour(), equalTo(RED)));
        berlinClock.getBottomHourPanel().stream().forEach(lamp -> assertThat(lamp.getColour(), equalTo(RED)));
    }

    private void verifyHourLampIllumination(Integer hour) {
        ZonedDateTime dateTime = ZonedDateTime.now().withHour(hour);

        if(hour >=5) {
            assertThat(berlinClock.getTopHourPanel().get(0).isOn(dateTime), is(true));
        } else {
            assertThat(berlinClock.getTopHourPanel().get(0).isOn(dateTime), is(false));
        }
        if(hour >=10) {
            assertThat(berlinClock.getTopHourPanel().get(1).isOn(dateTime), is(true));
        } else {
            assertThat(berlinClock.getTopHourPanel().get(1).isOn(dateTime), is(false));
        }
        if(hour >=15) {
            assertThat(berlinClock.getTopHourPanel().get(2).isOn(dateTime), is(true));
        } else {
            assertThat(berlinClock.getTopHourPanel().get(2).isOn(dateTime), is(false));
        }
        if(hour >=20) {
            assertThat(berlinClock.getTopHourPanel().get(3).isOn(dateTime), is(true));
        } else {
            assertThat(berlinClock.getTopHourPanel().get(3).isOn(dateTime), is(false));
        }

        switch (hour) {
            case 1:
            case 6:
            case 11:
            case 16:
            case 21: {
                assertThat(berlinClock.getBottomHourPanel().get(0).isOn(dateTime), is(true));
                assertThat(berlinClock.getBottomHourPanel().get(1).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(2).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(3).isOn(dateTime), is(false));
                break;
            }
            case 2:
            case 7:
            case 12:
            case 17:
            case 22: {
                assertThat(berlinClock.getBottomHourPanel().get(0).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(1).isOn(dateTime), is(true));
                assertThat(berlinClock.getBottomHourPanel().get(2).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(3).isOn(dateTime), is(false));
                break;
            }
            case 3:
            case 8:
            case 13:
            case 18:
            case 23: {
                assertThat(berlinClock.getBottomHourPanel().get(0).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(1).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(2).isOn(dateTime), is(true));
                assertThat(berlinClock.getBottomHourPanel().get(3).isOn(dateTime), is(false));
                break;
            }
            case 4:
            case 9:
            case 14:
            case 19:
            case 24: {
                assertThat(berlinClock.getBottomHourPanel().get(0).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(1).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(2).isOn(dateTime), is(false));
                assertThat(berlinClock.getBottomHourPanel().get(3).isOn(dateTime), is(true));
                break;
            }

        }

    }
}
