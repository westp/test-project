package com.west.berlin.clock.lamp;

import lombok.Getter;

import java.time.ZonedDateTime;
import java.util.function.Predicate;

public abstract class Lamp {

    @Getter
    private Colour colour;
    private Predicate<Integer> lightOnExpression;

    public Lamp(Colour colour, Predicate<Integer> lightOnExoression) {
        this.colour = colour;
        this.lightOnExpression = lightOnExoression;
    }

    public boolean isOn(ZonedDateTime dateTime) {
        return lightOnExpression.test(getDateField(dateTime));
    }

    protected abstract int getDateField(ZonedDateTime dateTime);
}
