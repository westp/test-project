package com.west.berlin.clock;

import com.west.berlin.clock.lamp.HourLamp;
import com.west.berlin.clock.lamp.Lamp;
import com.west.berlin.clock.lamp.MinuteLamp;
import com.west.berlin.clock.lamp.SecondLamp;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.west.berlin.clock.lamp.Colour.YELLOW;
import static com.west.berlin.clock.lamp.Colour.RED;

@Getter
public class BerlinClock {

    private Lamp secondLamp;
    private List<Lamp> topHourPanel;
    private List<Lamp> bottomHourPanel;
    private List<MinuteLamp> topMinutePanel;
    private List<MinuteLamp> bottomMinutePanel;

    public BerlinClock() {
        secondLamp = new SecondLamp(s -> s % 2 ==0);

        topHourPanel = new ArrayList<>();
        topHourPanel.add(new HourLamp(h -> h >= 5));
        topHourPanel.add(new HourLamp(h -> h >= 10));
        topHourPanel.add(new HourLamp(h -> h >= 15));
        topHourPanel.add(new HourLamp(h -> h >= 20));

        bottomHourPanel = new ArrayList<>();
        bottomHourPanel.add((new HourLamp(h-> h %5 == 1)));
        bottomHourPanel.add((new HourLamp(h-> h %5 == 2)));
        bottomHourPanel.add((new HourLamp(h-> h %5 == 3)));
        bottomHourPanel.add((new HourLamp(h-> h %5 == 4)));

        topMinutePanel = new ArrayList<>();
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 5));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 10));
        topMinutePanel.add(new MinuteLamp(RED, m -> m >= 15));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 20));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 25));
        topMinutePanel.add(new MinuteLamp(RED, m -> m >= 30));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 35));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 40));
        topMinutePanel.add(new MinuteLamp(RED, m -> m >= 45));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 50));
        topMinutePanel.add(new MinuteLamp(YELLOW, m -> m >= 55));

        bottomMinutePanel = new ArrayList<>();
        bottomMinutePanel.add((new MinuteLamp(YELLOW, h-> h %5 == 1)));
        bottomMinutePanel.add((new MinuteLamp(YELLOW, h-> h %5 == 2)));
        bottomMinutePanel.add((new MinuteLamp(YELLOW, h-> h %5 == 3)));
        bottomMinutePanel.add((new MinuteLamp(YELLOW, h-> h %5 == 4)));
    }
}
