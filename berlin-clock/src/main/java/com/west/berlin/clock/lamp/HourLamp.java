package com.west.berlin.clock.lamp;

import java.time.ZonedDateTime;
import java.util.function.Predicate;

import static com.west.berlin.clock.lamp.Colour.RED;

public class HourLamp extends Lamp {

    public HourLamp(Predicate<Integer> lightOnExoression) {
        super(RED, lightOnExoression);
    }

    @Override
    protected int getDateField(ZonedDateTime dateTime) {
        return dateTime.getHour();
    }
}
