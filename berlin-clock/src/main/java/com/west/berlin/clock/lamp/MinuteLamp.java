package com.west.berlin.clock.lamp;

import java.time.ZonedDateTime;
import java.util.function.Predicate;

public class MinuteLamp extends Lamp {

    public MinuteLamp(Colour colour, Predicate<Integer> lightOnExoression) {
        super(colour,lightOnExoression);
    }

    @Override
    protected int getDateField(ZonedDateTime dateTime) {
        return dateTime.getMinute();
    }
}
