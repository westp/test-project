package com.west.berlin.clock.lamp;

import java.time.ZonedDateTime;
import java.util.function.Predicate;

import static com.west.berlin.clock.lamp.Colour.YELLOW;

public class SecondLamp extends Lamp {

    public SecondLamp(Predicate<Integer> lightOnExoression) {
        super(YELLOW, lightOnExoression);
    }

    @Override
    protected int getDateField(ZonedDateTime dateTime) {
        return dateTime.getSecond();
    }
}
